import 'dart:async';
import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_radio/flutter_radio.dart';
import 'package:intl/intl.dart';
import 'package:radio_ar/app_bar.dart';
import 'package:radio_ar/drawer.dart';
import 'package:radio_ar/models/model_banner.dart';
import 'package:radio_ar/models/model_program.dart';
import 'package:radio_ar/models/model_stream_route.php.dart';
import 'package:radio_ar/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Player extends StatefulWidget {
  Player({Key key}) : super(key: key);
  @override
  _Player createState() => _Player();
}

class _Player extends State<Player> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  // final AudioPlayer player;
  // static const streamUrl = "https://live.nagaswarafm.com:9900/stream";
  final globalKey = GlobalKey<ScaffoldState>();
  bool isPlaying = false;
  String streamUrl;

  Future<ModelProgram> getModelProgram;
  Future<ModelBanner> getModelBanner;
  String _timeString;

  Timer _timer;

  int currentProgram = 0;
  // List<String> imgList;
  List<String> imgList = [];

  void initState() {
    super.initState();

    print('is playing $isPlaying');
    if (!isPlaying) {
      _getStreamRoute().then((result) {
        // print(result.stream);
        result.stream.forEach((e) {
          streamUrl = e.api;
          print('set prefs');
          // streamUrl = "https://live.nagaswarafm.com:9900/stream";
          // streamUrl = "https://live.phoenixradiobali.com:8250/phoenixradio";
          _setPrefs();
          // print(e.api);
        });

        FlutterRadio.playOrPause(url: streamUrl);
        audioStart();
      });
    }

    getModelProgram = _getData();
    getModelBanner = _getBanner();
    if (_timeString == null) {
      _getTime();
    }
    _timer = Timer.periodic(Duration(minutes: 1), (Timer t) => _getTime());
  }

  Future<void> audioStart() async {
    await FlutterRadio.audioStart();
    playingStatus();
    print('Audio Start OK');
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _timer.cancel();
    super.dispose();
  }

  void _getTime() {
    final String formattedDateTime =
        // DateFormat('yyyy-MM-dd \n kk:mm:ss').format(DateTime.now()).toString();
        DateFormat('kk:mm').format(DateTime.now());
    setState(() {
      _timeString = formattedDateTime;
      // print(_timeString);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: globalKey,
      appBar: appBar(),
      body: Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(255, 134, 127, 1.0),
        ),
        child: Container(
          margin: EdgeInsets.all(17),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: FutureBuilder<ModelBanner>(
                        future: getModelBanner,
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            var data = snapshot.data.bannerlist;

                            imgList = new List<String>();
                            for (int i = 0; i < data.length; i++) {
                              // for (int i = 1; i <= 6; i++) {
                              imgList.add(
                                  "https://radioarbali.com/images/banner/${data[i].image}");
                              // imgList.add("https://radioarbali.com/images/slide/${i}.jpg");
                              // print(
                              //     "https://phoenixradiobali.com/images/slide/${data[i].image}");
                            }
                            // print(getModelBanner);

                            return CarouselSlider(
                              options: CarouselOptions(
                                aspectRatio: 2.0,
                                enlargeCenterPage: true,
                                scrollDirection: Axis.horizontal,
                                autoPlay: true,
                              ),
                              items: imageSliders(),
                            );
                          } else if (snapshot.hasError) {
                            return Text("${snapshot.error}");
                          }
                          return Center(child: CircularProgressIndicator());
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Column(
                // mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  // Column(
                  //   children: [
                  // Center(
                  FutureBuilder<ModelProgram>(
                    future:
                        getModelProgram, // here get_datacall()  can be call directly
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        var program = snapshot.data.program;
                        // print(program.length);
                        for (var i = 0; i < program.length; i++) {
                          var startTime = program[i].mulai.split(":");
                          var endTime = program[i].akhir.split(":");
                          int startToMinutes = int.parse(startTime[0]) * 60 +
                              int.parse(startTime[1]);
                          int endToMinutes = int.parse(endTime[0]) * 60 +
                              int.parse(endTime[1]);
                          var currentTime = _timeString.split(":");
                          int ctimeToMinutes = int.parse(currentTime[0]) * 60 +
                              int.parse(currentTime[1]);

                          if (startToMinutes <= ctimeToMinutes) {
                            currentProgram = i;
                          }
                        }

                        return Column(
                          children: [
                            Row(
                              // mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Expanded(
                                  child: Text(
                                    program[currentProgram].acara,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20.0,
                                    ),
                                    textAlign: TextAlign.start,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  program[currentProgram].host,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 22.0,
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    program[currentProgram].mulai,
                                    style: TextStyle(
                                        color: Colors.yellow, fontSize: 20.0),
                                  ),
                                  Text(
                                    ' - ${program[currentProgram].akhir}',
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 20.0,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        );
                      } else if (snapshot.hasError) {
                        return Text(
                          "error",
                          style: TextStyle(color: Colors.white, fontSize: 20.0),
                        );
                      }
                      // By default, show a loading spinner
                      return Container(
                        height: 20,
                        width: 20,
                        child: CircularProgressIndicator(),
                      );
                    },
                  ),
                ],
              ),
              Expanded(
                // height: 200,
                // mainAxisAlignment: MainAxisAlignment.end,
                child: FutureBuilder<ModelProgram>(
                  future:
                      getModelProgram, // here get_datacall()  can be call directly
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      var program = snapshot.data.program;
                      // print(program.length);
                      return Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white60,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: ListView.separated(
                            padding: const EdgeInsets.all(8),
                            itemCount: program.length,
                            itemBuilder: (BuildContext context, int index) {
                              // print(program[index]);
                              if (currentProgram >= index) return Container();
                              return Container(
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Text(
                                            program[index].acara,
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline3,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Expanded(
                                          child: Text(program[index].host),
                                        ),
                                        Text(program[index].mulai),
                                        Text(' - ${program[index].akhir}'),
                                      ],
                                    ),
                                  ],
                                ),
                              );
                            },
                            separatorBuilder:
                                (BuildContext context, int index) {
                              if (currentProgram > index) return Container();
                              return const Divider();
                            },
                          ),
                        ),
                      );
                    } else if (snapshot.hasError) {
                      return Text(
                        "error",
                        style: TextStyle(color: Colors.white, fontSize: 20.0),
                      );
                    }
                    // By default, show a loading spinner
                    return Container();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      // bottomNavigationBar: _bottomAppBar(globalKey, streamUrl),
      bottomNavigationBar: BottomAppBar(
        // color: Colors.white,
        child: Container(
          decoration: BoxDecoration(
            color: Color.fromRGBO(255, 82, 82, 1.0),
          ),
          // padding: EdgeInsets.all(5),
          child: SafeArea(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: GestureDetector(
                    onTap: () {
                      // Scaffold.of(context).openDrawer();
                      // final snackBar = SnackBar(content: Text('Profile saved'));
                      globalKey.currentState.openDrawer();
                      print("btn menu clicked");
                    },
                    child: Image.asset(
                      "images/btn_menu.png",
                      fit: BoxFit.contain,
                      height: 35,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: GestureDetector(
                    onTap: () {
                      // get data and assign to model class
                      playingStatus();
                      FlutterRadio.playOrPause(url: streamUrl);
                      // playingStatus();
                      print("btn play clicked ${streamUrl}");
                    },
                    child: Image.asset(
                      // "images/btn_play.png",
                      !isPlaying
                          ? "images/btn_play.png"
                          : "images/btn_pause.png",
                      fit: BoxFit.contain,
                      height: 55,
                    ),
                  ),
                  // child: Container(),
                ),
              ],
            ),
          ),
        ),
      ),
      drawer: drawer(context),
    );
  }

  Future<ModelProgram> _getData() {
    print(" get data program using http");
    // call servicewrapper function
    servicewrapper wrapper = new servicewrapper();
    DateTime date = DateTime.now();
    date = date.add(Duration(days: 1));
    // print(date.weekday.toString());
    // print(DateFormat('EEEE').format(date));
    return wrapper.getProgram(date.weekday.toString());
    //get_prodlist
    // To display the data on screen, use the FutureBuilder widget.
    // The FutureBuilder widget comes with Flutter and makes it easy to work with asynchronous data sources
    // The Future you want to work with. In this case, the future returned from the  getProdCall() function.
    //A builder function that tells Flutter what to render, depending on the state of the Future: loading, success, or error.
  }

  Future<ModelBanner> _getBanner() {
    print(" get data banner using http");
    // call servicewrapper function
    servicewrapper wrapper = new servicewrapper();
    return wrapper.getBanner();
  }

  List<Widget> imageSliders() {
    return imgList
        .map((item) => Container(
              child: Container(
                // margin: EdgeInsets.all(5.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  child: Stack(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(0, 0, 0, 0.5),
                        ),
                      ),
                      // Image.network(item, fit: BoxFit.cover, width: 1000.0),
                      Positioned(
                        top: 17.0,
                        // bottom: 0.0,
                        left: 0.0,
                        right: 0.0,
                        child: Image.network(item,
                            fit: BoxFit.cover, width: 1000.0),
                        // child: Container(
                        //   decoration: BoxDecoration(
                        //     gradient: LinearGradient(
                        //       colors: [
                        //         Color.fromARGB(200, 0, 0, 0),
                        //         Color.fromARGB(0, 0, 0, 0)
                        //       ],
                        //       begin: Alignment.bottomCenter,
                        //       end: Alignment.topCenter,
                        //     ),
                        //   ),
                        //   padding: EdgeInsets.symmetric(
                        //       vertical: 10.0, horizontal: 20.0),
                        //   child: Text(
                        //     'No. ${imgList.indexOf(item)} image',
                        //     style: TextStyle(
                        //       color: Colors.white,
                        //       fontSize: 20.0,
                        //       fontWeight: FontWeight.bold,
                        //     ),
                        //   ),
                        // ),
                      ),
                    ],
                  ),
                ),
              ),
            ))
        .toList();
  }

  Future<ModelStreamRoute> _getStreamRoute() {
    print(" get data stream route using http");
    servicewrapper wrapper = new servicewrapper();
    return wrapper.getStreamRoute();
  }

  Future<void> _setPrefs() async {
    final SharedPreferences prefs = await _prefs;
    prefs.setString("streamUrl", streamUrl);
    // setState(() {
    // return prefs.setString("streamUrl", streamUrl).then((bool success) {});
    // });
  }

  Future playingStatus() async {
    // final SharedPreferences prefs = await _prefs;
    // prefs.setBool("isPlaying", isPlaying == 0 ? 1 : 0);

    print('is playing 1 $isPlaying');
    bool isP = await FlutterRadio.isPlaying();
    setState(() {
      isPlaying = isP;
      print('is playing 2 $isPlaying');
    });
  }
}
