class ModelProgram {
  List<Program> program;
  int success;

  ModelProgram({this.program, this.success});

  ModelProgram.fromJson(Map<String, dynamic> json) {
    if (json['program'] != null) {
      program = new List<Program>();
      json['program'].forEach((v) {
        program.add(new Program.fromJson(v));
      });
    }
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.program != null) {
      data['program'] = this.program.map((v) => v.toJson()).toList();
    }
    data['success'] = this.success;
    return data;
  }
}

class Program {
  String mulai;
  String akhir;
  String acara;
  String host;

  Program({this.mulai, this.akhir, this.acara, this.host});

  Program.fromJson(Map<String, dynamic> json) {
    mulai = json['mulai'];
    akhir = json['akhir'];
    acara = json['acara'];
    host = json['host'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['mulai'] = this.mulai;
    data['akhir'] = this.akhir;
    data['acara'] = this.acara;
    data['host'] = this.host;
    return data;
  }
}
