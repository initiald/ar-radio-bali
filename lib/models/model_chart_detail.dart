class ModelChartDetail {
  List<Chartlistdetail> chartlistdetail;
  int success;

  ModelChartDetail({this.chartlistdetail, this.success});

  ModelChartDetail.fromJson(Map<String, dynamic> json) {
    if (json['chartlistdetail'] != null) {
      chartlistdetail = new List<Chartlistdetail>();
      json['chartlistdetail'].forEach((v) {
        chartlistdetail.add(new Chartlistdetail.fromJson(v));
      });
    }
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.chartlistdetail != null) {
      data['chartlistdetail'] =
          this.chartlistdetail.map((v) => v.toJson()).toList();
    }
    data['success'] = this.success;
    return data;
  }
}

class Chartlistdetail {
  String idchartlist;
  String judul;
  String penyanyi;
  String rangking;
  String itunes;
  String flw;
  String fwoc;
  String fpp;

  Chartlistdetail({
    this.idchartlist,
    this.judul,
    this.penyanyi,
    this.rangking,
    this.itunes,
    this.flw,
    this.fwoc,
    this.fpp,
  });

  Chartlistdetail.fromJson(Map<String, dynamic> json) {
    idchartlist = json['idchartlist'];
    judul = json['judul'];
    penyanyi = json['penyanyi'];
    rangking = json['rangking'];
    itunes = json['itunes'];
    flw = json['flw'];
    fwoc = json['fwoc'];
    fpp = json['fpp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idchartlist'] = this.idchartlist;
    data['judul'] = this.judul;
    data['penyanyi'] = this.penyanyi;
    data['rangking'] = this.rangking;
    data['itunes'] = this.itunes;
    data['flw'] = this.flw;
    data['fwoc'] = this.fwoc;
    data['fpp'] = this.fpp;
    return data;
  }
}
