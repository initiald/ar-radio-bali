class ModelStreamRoute {
  List<Stream> stream;
  int success;

  ModelStreamRoute({this.stream, this.success});

  ModelStreamRoute.fromJson(Map<String, dynamic> json) {
    if (json['stream'] != null) {
      stream = new List<Stream>();
      json['stream'].forEach((v) {
        stream.add(new Stream.fromJson(v));
      });
    }
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.stream != null) {
      data['stream'] = this.stream.map((v) => v.toJson()).toList();
    }
    data['success'] = this.success;
    return data;
  }
}

class Stream {
  String id;
  String api;
  String radio;
  String isActive;

  Stream({this.id, this.api, this.radio, this.isActive});

  Stream.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    api = json['api'];
    radio = json['radio'];
    isActive = json['is_active'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['api'] = this.api;
    data['radio'] = this.radio;
    data['is_active'] = this.isActive;
    return data;
  }
}
