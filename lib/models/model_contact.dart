class ModelContact {
  List<Contact> contact;
  int success;

  ModelContact({this.contact, this.success});

  ModelContact.fromJson(Map<String, dynamic> json) {
    if (json['contact'] != null) {
      contact = new List<Contact>();
      json['contact'].forEach((v) {
        contact.add(new Contact.fromJson(v));
      });
    }
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.contact != null) {
      data['contact'] = this.contact.map((v) => v.toJson()).toList();
    }
    data['success'] = this.success;
    return data;
  }
}

class Contact {
  String facebook;
  String twitter;
  String contactNumber;
  String email;
  String sms;
  String alamat;

  Contact(
      {this.facebook,
      this.twitter,
      this.contactNumber,
      this.email,
      this.sms,
      this.alamat});

  Contact.fromJson(Map<String, dynamic> json) {
    facebook = json['facebook'];
    twitter = json['twitter'];
    contactNumber = json['contact_number'];
    email = json['email'];
    sms = json['sms'];
    alamat = json['alamat'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['facebook'] = this.facebook;
    data['twitter'] = this.twitter;
    data['contact_number'] = this.contactNumber;
    data['email'] = this.email;
    data['sms'] = this.sms;
    data['alamat'] = this.alamat;
    return data;
  }
}
