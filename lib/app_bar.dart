import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

AppBar appBar() {
  return AppBar(
    automaticallyImplyLeading: false, // hide default nav
    backgroundColor: Color.fromRGBO(
        255, 82, 82, 1.0), // 0.0 = transparant, 1.0 = fully opaque
    // flexibleSpace: Image.network(
    //   'https://radioarbali.com/images/logo.png', //https://radioarbali.com/images/logo.png
    //   // fit: BoxFit.fitHeight,
    //   height: 80,
    // ),
    title: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      // crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Flexible(
          child: Image.network(
            'https://radioarbali.com/images/logo.png',
            // fit: BoxFit.fitHeight,
            height: 50,
          ),
          flex: 0,
        ),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Radio AR',
                style: TextStyle(
                  color: Colors.white,
                  // fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text('Bali'),
            ],
          ),
          flex: 2,
        ),
        Flexible(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Container(
                  // color: Colors.white,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      color: Colors.white,
                      width: 1.0,
                      style: BorderStyle.solid,
                    ),
                    borderRadius: BorderRadius.circular(5.5),
                  ),
                  child: GestureDetector(
                    onTap: () {
                      _launchURL(
                          "https://instagram.com/radioarfmbali?igshid=1whn07c7nuuez");
                    },
                    child: Image.network(
                      "https://radioarbali.com/images/icon/icon_instagram_64.png",
                      fit: BoxFit.contain,
                      height: 30,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: GestureDetector(
                  onTap: () {
                    _launchURL(
                        "https://www.facebook.com/pages/RADIO-AR-BALI/318035824892396");
                  },
                  child: Image.network(
                    "https://radioarbali.com/images/icon/icon_facebook_64.png",
                    fit: BoxFit.contain,
                    height: 30,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 5.0, top: 5.0, right: 0.0, bottom: 5.0),
                child: GestureDetector(
                  onTap: () {
                    _launchURL("https://twitter.com/@radioar_bali");
                  },
                  child: Image.network(
                    "https://radioarbali.com/images/icon/icon_twitter_64.png",
                    fit: BoxFit.contain,
                    height: 30,
                  ),
                ),
              ),
            ],
          ),
          flex: 3,
        ),
      ],
    ),
  );
}

_launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url, forceWebView: true);
  } else {
    throw 'Could not launch $url';
  }
}
